# TypeORM + JWT

Essa api foi desenvolvida com objetivos acadêmicos, proposta pela Kenzie Academy.

## Como instalar?

Faça o clone do repositório:

<https://gitlab.com/alesilvasp/typeorm_crud_user>

Entre na pasta e rode o comando no terminal para instalar as dependências:

`yarn install`

Depois de instaladas as dependências, rode o comando no terminal:

`docker-compose up`

e em outro terminal, rode:

`yarn dev`

ou

`npm run dev`

O sistema estará rodando em https://localhost:3000

## Utilização

Para utilizar este sistema, é necessário utilizar um API Client, como o Insomnia ou Postman

### Rotas

**POST /users**

Feita para cadastrar um novo usuário

RESPONSE STATUS -> HTTP 201(CREATED)

Body:

```json
{
  "name": "Admin",
  "isAdmin": true,
  "email": "admin@admin.com",
  "password": "12345678"
}
```

Response:

```json
{
  "name": "Admin",
  "email": "admin@admin.com",
  "isAdmin": true,
  "id": "d8385c7a-a5ca-4e40-81cf-18ac744a11cc",
  "createdAt": "2022-01-28T20:24:16.100Z",
  "updatedAt": "2022-01-28T20:24:16.100Z"
}
```

**POST /login**

Logar na api informando _email_ e _password_

RESPONSE STATUS -> HTTP 200(OK)

Body:

```json
{
  "email": "admin@admin.com",
  "password": "12345678"
}
```

Response:

```json
{
  "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFsZXhhbmRlciIsImlhdCI6MTY0MjA4NTIzOCwiZXhwIjoxNjQyMDg4ODM4fQ.t8RmmoX4PDN817xf2p0dgwch0JoV0COpiqAigz1p7fw"
}
```

**GET /users**

Retorno de todos os usuários cadastrados

Authorization Header:

**Necessário passar o token para autenticação**

**Somente o Administrador tem acesso a rota**

RESPONSE STATUS -> HTTP 200(OK)

Body:

_No body_

Response:

```json
[
  {
    "id": "feca70c5-0370-4123-8a32-2c35e624fd29",
    "name": "Admin",
    "email": "admin@admin.com",
    "password": "$2a$10$eY5TpH5RdEh7TXmv0qopROvuvotsNQLpu8QT6Jb4YwIb/iJrAN4fu",
    "isAdmin": true,
    "createdAt": "2022-01-28T20:38:07.480Z",
    "updatedAt": "2022-01-28T20:38:07.480Z"
  },
  {
    "id": "e200caac-5fba-4b10-bd4d-5d80d397219b",
    "name": "Not admin",
    "email": "not@admin.com",
    "password": "$2a$10$L9w68tsllv3UzdcNWn/s4eJ36/HfyBGn7FPGY35itTdCWBWI5HKIu",
    "isAdmin": false,
    "createdAt": "2022-01-28T20:38:21.914Z",
    "updatedAt": "2022-01-28T20:38:21.914Z"
  }
]
```

Caso o usuário não seja administrador, o retorno será:

RESPONSE STATUS -> HTTP 401(Unauthorized)

```json
{
  "message": "Unauthorized"
}
```

**GET /users/profile**

Rota que retorna os dados do usuário logado, de acordo com o token

Authorization Header:

**Necessário passar o token para autenticação e autorização**

RESPONSE STATUS -> HTTP 200(Ok)

Body:

_No body_

Response:

```json
{
  "id": "feca70c5-0370-4123-8a32-2c35e624fd29",
  "name": "Admin",
  "email": "admin@admin.com",
  "isAdmin": true,
  "createdAt": "2022-01-28T20:38:07.480Z",
  "updatedAt": "2022-01-28T20:38:07.480Z"
}
```

**PATCH /users/:id**

Rota para alterações de dados, exceto _isAdmin_, passando o id por parâmetro.

**Somente o Administrador pode alterar os dados de qualquer usuário**

**Se o usuário _isAdmin_ for _false_, ele poderá alterar somente os dados de seu cadastro**

Authorization Header:

**Necessário passar o token para autenticação**

RESPONSE STATUS -> HTTP 200(Ok)

Body:

````json
{
  "name": "AnotherAdmin",
  "email": "anotherEmail@admin.com",
  "password": "anotherPass"
}

Response:

```json
{
	"id": "ae10982c-6ae4-4c2d-a701-7f0c1e8bc5d8",
	"name": "AnotherAdmin",
	"email": "anotherEmail@admin.com",
	"password": "$2a$10$RZpeF4bYQpuuNOncMbti5.p7BQjIhh/Ff9CKeAkcFT1Zwc4G1Ovpy",
	"isAdmin": true,
	"createdAt": "2022-01-28T18:14:37.145Z",
	"updatedAt": "2022-01-28T20:22:26.399Z"
}
````

Caso o usuário não seja administrador e queira atualizar dados de outro usuário, o retorno será:

RESPONSE STATUS -> HTTP 401(Unauthorized)

```json
{
  "message": "Unauthorized"
}
```

**DELETE /users/:id**

Rota para deleção de um usuário, passando o id por parâmetro.

**Somente o Administrador pode deletar qualquer usuário**

**Se o usuário _isAdmin_ for _false_, ele poderá deletar somente o seu cadastro**

Authorization Header:

**Necessário passar o token para autenticação**

RESPONSE STATUS -> HTTP 200(Ok)

Body:

_No body_

Response:

```json
{
  "message": "User deleted with success"
}
```

Caso o usuário não seja administrador e queira deletar o cadastro de outro usuário, o retorno será:

RESPONSE STATUS -> HTTP 401(Unauthorized)

```json
{
  "message": "Unauthorized"
}
```

## Tecnologias e linguagens utilizadas

- TypeScript
- Express.js
- Docker
- TypeORM
