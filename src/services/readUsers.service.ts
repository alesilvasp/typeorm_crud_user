import { UsersRepository } from "../repositories/users.repository";
import { getCustomRepository } from "typeorm";
import { authenticateUser } from "../middlewares/autenticate.middleware";
import { isAdmin } from "../middlewares/isAdmin.middleware";

class ReadUserService {
  async execute(token: string) {
    const usersRepository = getCustomRepository(UsersRepository);

    const isAutenticate = authenticateUser(token);

    if (isAutenticate === undefined) {
      throw Error("Missing authorization headers");
    }

    if (!isAdmin(token)) {
      throw Error("Unauthorized");
    }

    const users = usersRepository.find();

    return users;
  }
}

export default ReadUserService;
