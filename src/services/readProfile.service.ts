import { authenticateUser } from "../middlewares/autenticate.middleware";

class ReadProfileService {
  async execute(token: string) {
    const isAutenticate = authenticateUser(token);

    if (isAutenticate === undefined) {
      throw Error("Missing authorization headers");
    }

    return isAutenticate.userData;
  }
}

export default ReadProfileService;
