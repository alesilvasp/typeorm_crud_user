import { UsersRepository } from "../repositories/users.repository";
import { getCustomRepository } from "typeorm";
import authenticateUser from "../middlewares/autenticate.middleware";

class DeleteUserService {
  async execute(token: string, id: string) {
    const usersRepository = getCustomRepository(UsersRepository);

    const isAutenticate = authenticateUser(token);

    if (isAutenticate === undefined) {
      throw Error("Missing authorization headers");
    }
    if (isAutenticate.userData.isAdmin) {
      await usersRepository.delete(id);
    } else {
      if (isAutenticate.userData.id === id) {
        await usersRepository.delete(id);
      } else {
        throw Error("Missing admin permissions");
      }
    }
    await usersRepository.delete(id);

    return "User deleted with success";
  }
}

export default DeleteUserService;
