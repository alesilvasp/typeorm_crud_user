import { getCustomRepository } from "typeorm";
import authenticateUser from "../middlewares/autenticate.middleware";
import { UsersRepository } from "../repositories/users.repository";

import * as bcrypt from "bcryptjs";
import { isAdmin } from "../middlewares/isAdmin.middleware";

interface IProps {
  id: string;
  data: {
    isAdmin?: boolean;
    password: string;
  };
}

class UpdateUserService {
  async execute(token: string, { id, data }: IProps) {
    const usersRepository = getCustomRepository(UsersRepository);

    const isAutenticate = authenticateUser(token);

    if (isAutenticate === undefined) {
      throw Error("Missing authorization headers");
    }

    const { isAdmin: user_isAdmin, ...dataWithoutIsAdmin } = data;
    if (data.password) {
      const newHashPassword = await bcrypt.hash(data.password, 10);
      dataWithoutIsAdmin.password = newHashPassword;
    }

    if (isAdmin(token)) {
      await usersRepository.update(id, dataWithoutIsAdmin);
    } else {
      if (isAutenticate.userData.id === id) {
        await usersRepository.update(id, dataWithoutIsAdmin);
      } else {
        throw Error("Missing admin permissions");
      }
    }

    const user = usersRepository.findOne({ id });

    return user;
  }
}

export default UpdateUserService;
