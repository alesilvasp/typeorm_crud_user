import * as bcrypt from "bcryptjs";
import dotenv from "dotenv";
import { getCustomRepository } from "typeorm";
import { UsersRepository } from "../repositories/users.repository";
import jwt from "jsonwebtoken";

dotenv.config();

const jwtConfig = {
  secret: <string>process.env.JWT_SECRET_KEY,
  expiresIn: "1h",
};

interface IUserRequest {
  email: string;
  password: string;
  isAdmin: boolean;
}

class LoginUserService {
  async execute({ email, password }: IUserRequest) {
    const usersRepository = getCustomRepository(UsersRepository);

    const user = await usersRepository.findOne({ email });

    if (user !== undefined) {
      const match = await bcrypt.compare(password, user.password);

      const { password: user_password, ...userData } = user;

      let token = jwt.sign(
        {
          userData,
        },
        jwtConfig.secret,
        {
          expiresIn: jwtConfig.expiresIn,
        }
      );
      if (match) {
        return { accessToken: token };
      } else {
        throw new Error("Wrong email/password");
      }
    } else {
      throw new Error("Wrong email/password");
    }
  }
}

export default LoginUserService;
