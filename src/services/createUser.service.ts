import { UsersRepository } from "../repositories/users.repository";
import { getCustomRepository } from "typeorm";

interface IUserRequest {
  name: string;
  email: string;
  password: string;
  isAdmin: boolean;
}

class CreateUserService {
  async execute({ name, email, password, isAdmin }: IUserRequest) {
    const usersRepository = getCustomRepository(UsersRepository);

    if (!email) {
      throw new Error("All fields must be filled");
    }

    const emailAlreadyExists = await usersRepository.findOne({ email });

    if (emailAlreadyExists) {
      throw new Error("E-mail already registered");
    }

    const user = usersRepository.create({ name, email, password, isAdmin });

    await usersRepository.save(user);

    return user;
  }
}

export default CreateUserService;
