import { Request, Response } from "express";
import ReadProfileService from "../services/readProfile.service";

class ReadProfileController {
  async handle(request: Request, response: Response) {
    const readProfileService = new ReadProfileService();

    let token = request.headers.authorization?.split(" ")[1];
    if (!token) {
      return response
        .status(401)
        .json({ message: "Missing authorization headers" });
    }
    try {
      const user = await readProfileService.execute(<string>token);

      return response.json(user);
    } catch (error) {
      return response.status(401).json({ message: (<Error>error).message });
    }
  }
}

export default ReadProfileController;
