import { Request, Response } from "express";
import LoginUserService from "../services/loginUser.service";

class LoginUserController {
  async handle(request: Request, response: Response) {
    try {
      const { email, password, isAdmin } = request.body;

      const loginUserService = new LoginUserService();

      const token = await loginUserService.execute({
        email,
        password,
        isAdmin,
      });

      return response.json(token);
    } catch (error) {
      return response.status(401).json({ message: (<Error>error).message });
    }
  }
}

export default LoginUserController;
