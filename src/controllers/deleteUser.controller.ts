import { Request, Response } from "express";
import DeleteUserService from "../services/deleteUser.service";

class DeleteUsersController {
  async handle(request: Request, response: Response) {
    const { id } = request.params;

    const token = request.headers.authorization?.split(" ")[1];
    if (!token) {
      return response
        .status(401)
        .json({ message: "Missing authorization headers" });
    }

    const deleteUserService = new DeleteUserService();

    try {
      const user = await deleteUserService.execute(token, id);
      response.status(200).json({ message: user });
    } catch (error) {
      return response.status(401).json({ message: (<Error>error).message });
    }
  }
}

export default DeleteUsersController;
