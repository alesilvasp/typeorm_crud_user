import { Request, Response } from "express";
import CreateUserService from "../services/createUser.service";

class CreateUserController {
  async handle(request: Request, response: Response) {
    try {
      const { name, email, password, isAdmin } = request.body;

      const createUserService = new CreateUserService();

      const user = await createUserService.execute({
        name,
        email,
        password,
        isAdmin,
      });

      const { password: user_password, ...userWithoutPassword } = user;

      return response.status(201).json(userWithoutPassword);
    } catch (error) {
      return response.status(400).json({message: (<Error> error).message});
    }
  }
}

export default CreateUserController;
