import { Request, Response } from "express";
import ListUsersService from "../services/readUsers.service";

class ReadUsersController {
  async handle(request: Request, response: Response) {
    const listUsersService = new ListUsersService();

    let token = request.headers.authorization?.split(" ")[1];
    if (!token) {
      return response
        .status(401)
        .json({ message: "Missing authorization headers" });
    }
    try {
      const users = await listUsersService.execute(<string>token);

      
      const usersList = users.map(item => {
        const { password: user_password, ...userWithoutPassword} = item
        return userWithoutPassword
      })

      return response.json(usersList);
    } catch (error) {
      return response.status(401).json({ message: (<Error>error).message });
    }
  }
}

export default ReadUsersController;
