import { Request, Response } from "express";
import { User } from "../entities/User";
import UpdateUserService from "../services/updateUser.service";

class UpdateUserController {
  async handle(request: Request, response: Response) {
    const { id } = request.params;
    const data = request.body;

    const updateUserService = new UpdateUserService();

    const token = request.headers.authorization?.split(" ")[1];
    if (!token) {
      return response
        .status(401)
        .json({ message: "Missing authorization headers" });
    }
    try {
      const updatedUser: any = await updateUserService.execute(token, { id, data });
      const { password: user_password, ...userWithoutPassword } = updatedUser
      response.status(200).json(userWithoutPassword);
    } catch (error) {
      return response.status(401).json({ message: (<Error>error).message });
    }
  }
}

export default UpdateUserController;
