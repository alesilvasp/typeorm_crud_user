import { Router } from "express";
import CreateUserController from "./controllers/createUser.controller";
import LoginUserController from "./controllers/loginUser.controller";
import ReadUsersController from "./controllers/readUsers.controller";
import ReadProfileController from "./controllers/readProfile.controller";
import UpdateUserController from "./controllers/updateUser.controller";
import DeleteUsersController from "./controllers/deleteUser.controller";

const router = Router();

const createUserController = new CreateUserController();
const loginUserController = new LoginUserController();
const readUserController = new ReadUsersController();
const readProfileController = new ReadProfileController();
const updateUserController = new UpdateUserController();
const deleteUserController = new DeleteUsersController();

router.post("/users", createUserController.handle);
router.post("/login", loginUserController.handle);
router.get("/users", readUserController.handle);
router.get("/users/profile", readProfileController.handle);
router.patch("/users/:id", updateUserController.handle);
router.delete("/users/:id", deleteUserController.handle);

export default router;
