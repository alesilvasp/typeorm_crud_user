import jwt from "jsonwebtoken";

const config = {
  secret: <string>process.env.JWT_SECRET_KEY,
  expiresIn: "1h",
};

export const isAdmin = (token: string): boolean => {
  const decodedJWT: any = jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      throw new Error("Invalid Token.");
    }
    return decoded;
  });
  if (decodedJWT.userData.isAdmin === true) {
    return true;
  } else {
    return false;
  }
};
