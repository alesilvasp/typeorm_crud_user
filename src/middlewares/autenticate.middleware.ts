import jwt, { JwtPayload } from "jsonwebtoken";
import dotenv from "dotenv";

dotenv.config();

const config = {
  secret: <string>process.env.JWT_SECRET_KEY,
  expiresIn: "1h",
};

export const authenticateUser = (token: string) => {
  const decodedJWT: any = jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      throw new Error("Invalid Token.");
    }
    return decoded;
  });
  return decodedJWT as JwtPayload;
};

export default authenticateUser;
